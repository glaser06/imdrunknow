//
//  FirstViewController.swift
//  NewYoApp
//
//  Created by Zaizen Kaegyoshi on 9/6/14.
//  Copyright (c) 2014 Zaizen Kaegyoshi. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
                            
    @IBOutlet weak var inputField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func addFriends(sender: AnyObject)
    {
        list.append(inputField.text)
        NSUserDefaults.standardUserDefaults().setObject(list, forKey: "UserLists")
        inputField.text = ""
        
    }
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
        
    }
    @IBAction func returnToView(sender: AnyObject)
    {
        self.tabBarController?.selectedIndex=0
    }


}

