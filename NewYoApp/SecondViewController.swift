//
//  SecondViewController.swift
//  NewYoApp
//
//  Created by Zaizen Kaegyoshi on 9/6/14.
//  Copyright (c) 2014 Zaizen Kaegyoshi. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
                            
    @IBOutlet weak var inputField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func setName(sender: AnyObject)
    {
        list[0]=inputField.text
        NSUserDefaults.standardUserDefaults().setObject(list, forKey: "UserLists")
        inputField.text=""
        self.view.endEditing(true)
        self.tabBarController?.selectedIndex=0
        //self.tabBarController?.tabBar.removeFromSuperview()
        
    }


}

