//
//  CViewController.swift
//  bubble_ios
//
//  Created by Gerry Zhou on 9/6/14.
//  Copyright (c) 2014 Gerry Zhou. All rights reserved.
//

import UIKit
import CoreLocation

class CViewController: UIViewController, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableFriends: UITableView!
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        locationManager.delegate = self
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        
        var locValue:CLLocationCoordinate2D = manager.location.coordinate
        
        //println("locations = \(locValue.latitude) \(locValue.longitude)")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        tableFriends.reloadData()
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if(editingStyle == UITableViewCellEditingStyle.Delete)
        {
            list.removeAtIndex(indexPath.row)
            NSUserDefaults.standardUserDefaults().setObject(list, forKey: "UserLists")
            tableFriends.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "test")
        
        cell.textLabel?.text = list[indexPath.row]
        
        
        //cell.setValue(taskMgr.tasks[indexPath.row], forKey: taskMgr.tasks[indexPath.row].desc)
        
        
        
        return cell
    }
    
    @IBAction func sendLocation(sender: AnyObject)
    {
        println("I am here")
        var locValue:CLLocationCoordinate2D = locationManager.location.coordinate
        //print(locationManager.location.coordinate.latitude)
        for people in list
        {
            var url = NSURL(string: "https://api.justyo.co/yo/")
            var request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "POST"
            
            var dataString = "api_token=1185bebf-4441-18d0-29d8-c9c3938d3bae&link=http://maps.google.com/?q=\(locValue.latitude),\(locValue.longitude)&username="+people
            //print(dataString)
                        
            var requestBodyData = dataString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
            let data = requestBodyData? .dataUsingEncoding(NSUTF8StringEncoding)
            request.HTTPBody = data
            
            var connection = NSURLConnection(request: request, delegate: self, startImmediately: false)
            
            println("sending request...")
            
            connection.start()
        }
        
        
        
    }
    @IBAction func addFriends(sender:AnyObject)
    {
        self.tabBarController?.selectedIndex=2
        
        
    }
    
    
}

